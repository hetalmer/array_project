//Find function is used to find the value with specific condition defined in cb function but this function returns only one single value not multiple value.
function find(list, cb) {
    if (Array.isArray(list) && typeof (list[0]) != "object") {
        for (let i = 0; i < list.length; i++) {
            ans = cb(list[i]);// If cb function returns true than that element will be return otherwise return undefined
            if (ans) return list[i];
        }
    }
}
module.exports = find;