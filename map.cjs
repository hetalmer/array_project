//Function to create new array based on call back function. It contains two argument one is list and another one is call back function
function map(list, cb) {
    let newArray = []
    if (Array.isArray(list)) {
        for (let index = 0; index < list.length; index++) {
            newArray.push(cb(list[index], index, list));// push elements into new array after calling cb function
        }
        return newArray;
    }
    return [];
}
module.exports = map;