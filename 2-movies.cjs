const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

function getEarningInNumber(earning){
    return parseInt(earning.replace(/[^0-9 ]/g,''));
}
//Q1. Find all the movies with total earnings more than $500M.
//In this function to get only number value from total earning I used pattern matching for it with using replace function
function moviesEarningMoreThan500(data){
    const movieEarning = (Object.entries(data)).filter((value)=>{
        const totalEarning = getEarningInNumber(value[1].totalEarnings);
        if(totalEarning > 500){
            return value;
        }
    });
    return Object.fromEntries(movieEarning);
} 
//console.log(moviesEarningMoreThan500(favouritesMovies));

//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
//This function same as above just add another condition with oscar nomination more than 3
function movieEarningWithOscarNominations(data){
    const movieEarningAndNomination = (Object.entries(data)).filter((value)=>{
        const totalEarning = getEarningInNumber(value[1].totalEarnings);
        const oscarNominations = value[1].oscarNominations;
        if(totalEarning>500 && oscarNominations > 3){
            return value;
        }
    });
    return Object.fromEntries(movieEarningAndNomination);
}
//console.log(movieEarningWithOscarNominations(favouritesMovies));

//Q.3 Find all movies of the actor "Leonardo Dicaprio".
//this function retruns the data of the actor name passed as an argument
function specificActiorMovie(data,actor){
    const specificActior = (Object.entries(data)).filter((value)=>{
        if(value[1].actors.includes(actor)){
            return value;
        }
    });
    return Object.fromEntries(specificActior);
}
//console.log(specificActiorMovie(favouritesMovies,'Leonardo Dicaprio'));


//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.
// I was stuck in this function because of my silly mistake
//In this function if rating is same than it compare total earning in same way
function sortByImdbAndTotalEarning(data){
    const sortData = (Object.entries(data)).sort((list1,list2)=>{
        const rating1 = list1[1].imdbRating;
        const rating2 = list2[1].imdbRating;
        if(rating1 > rating2){
            return -1;
        }
        else if(rating1 < rating2){
            return 1;
        }
        else{
            const earning1 = getEarningInNumber(list1[1].totalEarnings);
            const earning2 = getEarningInNumber(list2[1].totalEarnings);
            if(earning1 > earning2){
                return -1;
            }
            else if(earning1 < earning2){
                return 1;
            }
            else{
                return 0
            }
    }
    });
    return Object.fromEntries(sortData);
}
//console.log(sortByImdbAndTotalEarning(favouritesMovies));
//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//drama > sci-fi > adventure > thriller > crime
//function to group by data by movie genre but if it is multiple follow the given sequence
function groupByGenre(data){
    const genreList = ['drama','sci-fi','adventure','thriller','crime'];
    const groupData = Object.entries(data).reduce((result,value)=>{
        genreData = genreList.filter(values=> value[1].genre.includes(values));// find the sequence of multiple genreDaa
        const subGroupData = genreData.map(values =>{ // map that data to get individual data and add to object values
            if(result.hasOwnProperty(values)){
                result[values] = Object.assign({},result[values],{[value[0]]:value[1]});
            }
            else
            {
                result[values] = {[value[0]]:value[1]};
            }
        });
    return result;
},{});
   return groupData;
}
console.log(groupByGenre(favouritesMovies));