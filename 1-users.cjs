const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 
//01 Find all users who are interested in playing video games.
function userInterestedInVideoGames(data){
    const videoGames = Object.entries(data).filter(value=>{
        const user = Object.fromEntries(Object.entries(value));
        if(user['1']['interests'].includes('Video Games'))
            return user;
    });
    return Object.fromEntries(videoGames);
}
//console.log(userInterestedInVideoGames(users));
//Q2 Find all users staying in Germany.

function userStayGermany(data){
    const stayGermany = Object.entries(data).filter(value=>{
        const user = Object.fromEntries(Object.entries(value));
        if(user['1']['nationality'] === 'Germany'){
            return user;
        }
    });
    return Object.fromEntries(stayGermany);
}
console.log(userStayGermany(users));
//Q4 Find all users with masters Degree.
function userWithMasterDegree(data){
    const masterDegree = Object.entries(data).filter(value=>{
        const user = Object.fromEntries(Object.entries(value));
        if((user['1']['qualification']) === 'Masters'){
            return user;
        }
    });
    return Object.fromEntries(masterDegree);
}
console.log(userWithMasterDegree(users));
//Q5 Group users based on their Programming language mentioned in their designation.
function groupUserByLanguage(data){
    const language = ['Golang','Javascript','Python'];
    const groupByLanguage = Object.entries(data).reduce((result,value)=>{
        const user = Object.fromEntries(Object.entries(value));
        const designation = user['1']['desgination'];
        console.log(designation);
        console.log(new RegExp(language.join('|')).test(designation));
        if(new RegExp(language.join('|')).test(designation)) {
            if(result.hasOwnProperty(user['1']['desgination'])){
                result[user['1']['desgination']].push(user);
            }
            else{
                result[user['1']['desgination']] = user;
            }
        }
        return result;
    },{});
    return Object.fromEntries(Object.entries(groupByLanguage));
}
console.log(groupUserByLanguage(users));