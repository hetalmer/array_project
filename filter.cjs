//Filter function filter the data which means that it returns all the data from the list which matches the condition of cb function. If not found it returns empty list
function filter(list, cb) {
    let newlist = []
    if (Array.isArray(list)) {
        for (let index = 0; index < list.length; index++) {
            let ans = cb(list[index], index, list); // if cb function returns true it add it in to the list
            if (ans === true) newlist.push(list[index]);
        }
    }
    return newlist;
}
module.exports = filter;