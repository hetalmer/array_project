//Program to test possible cases of reduce function
const minVal = (a, b) => Math.min(a, b); // create min function to calculate minimum value
const reduce = require("../reduce.cjs")
console.log(reduce([10, 20, 30], function (a, b) { return a + b }, 11)); // combine all elements with initial value 10
console.log(reduce([11, 12, 13], minVal, 8));// find mininum from all elements with initial value 8
console.log(reduce([11, 12, 13], minVal));// find mininum from list without initial value
console.log(reduce([], minVal, 1));// find minimum without list but initial value
console.log(reduce([], minVal))// it doesn't give any output because if list is empty than starting value must be needed.
console.log(reduce(1, minVal));// to execute reduce() it requires array 
