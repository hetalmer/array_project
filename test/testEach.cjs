const each = require('../each.cjs');
each([1, 2, 3, 4], function (value, index) { console.log(index, value) });
each(['one', 'two', 'three', 'four'], (word) => {
    console.log(word);
});
each([2, 4, 6, 8], (no, index) => console.log(no * index));