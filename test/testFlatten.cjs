// Test flattern function with different values and depth
const flatten = require('../flatten.cjs');
console.log(flatten([1, 2, [3, 4], [[5], 6]], 2));
console.log(flatten([1, [[[[2], 3]]], 5, 6], 3));
console.log(flatten([], 4));
console.log(flatten([[[[[[[[[[1]]]]]]]]]], Infinity));
console.log(flatten([1, [[[[2], 3]]], [[5]], [[6]]], Infinity));