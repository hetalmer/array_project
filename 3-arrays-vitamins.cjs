const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/
function getAvailableItems(data) {
    const result = data.filter(value => value.available);
    return result;
}
console.log(getAvailableItems(items));
function getVitaminCItems(data) {
    const result = data.filter(value => value['contains'] === 'Vitamin C');
    return result;
}
console.log(getVitaminCItems(items));
function getVitaminAItems(data) {
    const result = data.filter(value => value['contains'].includes('Vitamin A'));
    return result;
}
console.log(getVitaminAItems(items));
function getVitaminsByGroup(data) {
    const groupData = data.reduce((result, value) => {
        const contain = value.contains.split(",");
        //let temp = result;
        //console.log(result);
        contain.forEach(element => {

            if (result.hasOwnProperty(element.trim())) {
                result[element.trim()].push(value.name);
            }
            else {
                result[element.trim()] = [value.name];
            }
        });
        return result;
    }, {});
    return groupData;
}
console.log(getVitaminsByGroup(items));
function sortData(items) {
    const result = items.sort((list1, list2) => {
        if (((list1.contains).split(',').length > (list2.contains).split(',').length)) {
            return -1;
        }
        else if (((list1.contains).split(',').length > (list2.contains).split(',').length)) {
            return 1;
        }
        else {
            return 0;
        }
    });
    return result;
}
console.log(sortData(items));