// Function which reduced the list passed as a argument based on callback function. It reduced from left to right
function reduce(list, callback, startingValue) {
    let initial = 0
    if (Array.isArray(list)) { // check that passed list is array or not
        if (!startingValue) { // if starting value is not defined consider first value as staring value
            startingValue = list[initial];
            initial = 1;
        }
        for (let index = initial; index < list.length; index++) {
            startingValue = callback(startingValue, list[index], index, list);// calling a callback function with starting and list values one by one
        }
        return startingValue; //return the reduced value
    }
}
module.exports = reduce;