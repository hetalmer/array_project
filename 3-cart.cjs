const products = [
	{
		shampoo: {
			price: "$50",
			quantity: 4,
		},
		"Hair-oil": {
			price: "$40",
			quantity: 2,
			sealed: true,
		},
		comb: {
			price: "$12",
			quantity: 1,
		},
		utensils: [
			{
				spoons: { quantity: 2, price: "$8" },
			},
			{
				glasses: { quantity: 1, price: "$70", type: "fragile" },
			},
			{
				cooker: { quantity: 4, price: "$900" },
			},
		],
		watch: {
			price: "$800",
			quantity: 1,
			type: "fragile",
		},
	},
];

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/
//01. Find all the items with price more than $65
function getItemsFilterByPrice(data, price) {
	let filterByPrice1 = Object.entries(data[0]).filter((values) => {
		if (Array.isArray(values["1"])) {
			// get object values and check if it contains array or not
			//if array create a array which is store price is greater than 65
			let subArray = [];
			values["1"].forEach((element) => {
				if (element[Object.keys(element)].price.replace("$", "") > price) {
					subArray.push(element);
				}
			});
			values["1"] = subArray;
			return values;
		} else {
			//if not array it chack and return it;
			if (Number(values["1"].price.replace("$", "")) > price) {
				return values;
			}
		}
	});
	return Object.fromEntries(filterByPrice1);
}
console.log(getItemsFilterByPrice(products, 65));
//02. Find all the items where quantity ordered is more than 1.
function getItemsQuantityOrder(data, quantity) {
	let filterByQuantity = Object.entries(data[0]).filter((values) => {
		//In this function use another filter for sub array
		if (Array.isArray(values["1"])) {
			const subData = values["1"].filter((value) => {
				if (value[Object.keys(value)].quantity > quantity) {
					return value;
				}
			});
			values["1"] = subData;
			return values;
		} else {
			if (values["1"].quantity > quantity) {
				return values;
			}
		}
	});
	return Object.fromEntries(filterByQuantity);
}
console.log(getItemsQuantityOrder(products, 1));
// Q.3 Get all items which are mentioned as fragile.
function getFragileItems(data) {
	let filterByFragile = Object.entries(data[0]).filter((values) => {
		if (Array.isArray(values["1"])) {
			const subData = values["1"].filter((value) => {
				if (value[Object.keys(value)].hasOwnProperty("type")) {
					if (value[Object.keys(value)].type === "fragile") {
						return values;
					}
				}
			});
			values["1"] = subData;
			return values;
		} else {
			if (values["1"].hasOwnProperty("type")) {
				if (values["1"].type === "fragile") {
					return values;
				}
			}
		}
	});
	return Object.fromEntries(filterByFragile);
}
console.log(getFragileItems(products));
// Q.4 Find the least and the most expensive item for a single quantity.
function mostExpensiveItem(data) {
	const expensiveItem = Object.entries(data[0]).filter((values) => {
		//In this function use another filter for sub array
		let subData = [];
		if (Array.isArray(values["1"])) {
			const subData = values["1"].filter((value) => {
				if (value[Object.keys(value)].quantity === 1) {
					return value;
				}
			});
			if ((subData.length = 1)) {
				values["1"] = subData;
			} else {
				subData
					.sort(
						(list1, list2) =>
							list2["price"].replace("$", "") - list1["price"].replace("$", ""),
					)
					.slice(1, 2);
				values["1"] = subData;
			}
			return values;
		} else {
			if (values["1"].quantity === 1) {
				return values;
			}
		}
	});
	return Object.fromEntries(expensiveItem);
}
console.log(mostExpensiveItem(products));
// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
