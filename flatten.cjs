//funtion to remove sub array upto specified depth. If user doen't specify any depth it will consider as a 1.
function flatten(list, depth = 1) {
    if (Array.isArray(list) && depth >= 0) {
        let newArray = [];
        let counter = 0;
        let flag = true;
        if (depth === 0) { // It removes sub array untill depth will be 0
            return list;//If depth is 0 it returns the array.
        }
        else {
            for (let index = 0; index < list.length; index++) {
                if (Array.isArray(list[index])) {
                    flag = false;
                    for (let subIndex = 0; subIndex < list[index].length; subIndex++) {
                        if (list[index][subIndex] !== undefined) {
                            newArray.push(list[index][subIndex]);
                        }
                    }
                }
                else {
                    if (list[index] !== undefined) {
                        newArray.push(list[index]);
                    }// if array element is array than it concate in newarray. Because concate function copy the content of second array to first array
                }
            }
            return flatten(newArray, flag ? 0 : depth - 1)
        }
    }
}
module.exports = flatten;