/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

const fs = require("fs");

//Q1. Create 2 files simultaneously (without chaining).
//Wait for 2 seconds and starts deleting them one after another. (in order)

function deleteFile(file) {
	//return promise for delete file
	return new Promise((resolve, reject) => {
		fs.unlink(file, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(file);
			}
		});
	});
}
function error(err) {
	console.log(err);
}
function createFile() {
	//put settimeout for 2 seconds to wait to write into file
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			for (let index = 1; index <= 2; index++) {
				fs.writeFile(`file${index}`, "{id:1,name:'hetal'}", function (err) {
					if (err) {
						console.log(err);
					}
				});
			}
			resolve("done");
		}, 2000);
	});
}
const deleteFileList = [];
createFile()
	.then(() => {
		for (let index = 1; index <= 2; index++) {
			deleteFileList.push(deleteFile(`file${index}`));
		}
	})
	.then(() => {
		return Promise.all(deleteFileList);
	})
	.then(() => {
		console.log("delete operation successful");
	})
	.catch((err) => {
		error(err);
	});

/*    Q2. Create a new file with lipsum data (you can google and get this). 
    Do File Read and write data to another file
    Delete the original file 
    Using promise chaining
*/
const promise = new Promise((resolve, reject) => {
	fs.readFile("lipsum.txt", "utf-8", function (err, data) {
		if (err) {
			reject("File not found");
		}
		resolve(data);
	});
});
promise
	.then(
		(data) =>
			new Promise((resolve, reject) => {
				fs.writeFile("copyLipsum.txt", data, function (err) {
					if (err) {
						reject(err);
					} else {
						resolve("copyLipsum.txt");
					}
				});
			}),
	)
	.then((msg) => {
		console.log(msg);
		deleteFile("lipsum.txt", (err) => {
			if (err) console.log(err);
		});
	})
	.catch((err) => {
		console.log(err);
	});

/*



*/
function login(user, val) {
	if (val % 2 === 0) {
		return Promise.resolve(user);
	} else {
		return Promise.reject(new Error("User not found"));
	}
}

function getData() {
	return Promise.resolve([
		{
			id: 1,
			name: "Test",
		},
		{
			id: 2,
			name: "Test 2",
		},
	]);
}

function logData(user, activity) {
	return new Promise((resolve, reject) => {});
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

//A. login with value 3 and call getData once login is successful

const data = login(`{id:1,name:'ddd'}`, 3);
data
	.then((data) => {
		return getData();
	})
	.then((data) => {
		console.log(data);
	})
	.catch((err) => console.log(err));
